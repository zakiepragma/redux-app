import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { saveProduct } from "../features/productSlice";

const AddProduct = () => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const createProduct = async (e) => {
    e.preventDefault();
    await dispatch(saveProduct({ title, price }));
    navigate("/");
  };

  return (
    <div style={{ width: "50%" }} className="container">
      <form onSubmit={createProduct} className="box mt-5">
        <p className="is-size-3 has-text-weight-bold">Add New Product</p>
        <hr />
        <div className="field">
          <label className="label">Title</label>
          <div className="control">
            <input
              type="text"
              className="input"
              placeholder="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Price</label>
          <div className="control">
            <input
              type="text"
              className="input"
              placeholder="Price"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
        </div>
        <div className="field is-flex is-justify-content-space-between">
          <button className="button is-success">Submit</button>
          <Link className="button" to="/">
            back to home
          </Link>
        </div>
      </form>
    </div>
  );
};

export default AddProduct;
