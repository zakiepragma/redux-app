import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  deleteProduct,
  getProducts,
  productSelectors,
} from "../features/productSlice";

const ShowProduct = () => {
  const dispatch = useDispatch();

  const products = useSelector(productSelectors.selectAll);
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);
  return (
    <div style={{ width: "75%" }} className="container">
      <div className="box mt-5">
        <div className="field is-flex is-justify-content-space-between">
          <p className="is-size-3 has-text-weight-bold">List Product</p>
          <Link to={"add"} className="button is-info">
            Add New
          </Link>
        </div>
        <hr />
        <table className="table is-striped is-fullwidth">
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Price</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product, index) => (
              <tr key={product.id}>
                <td>{index + 1}</td>
                <td>{product.title}</td>
                <td>{product.price}</td>
                <td>
                  <Link
                    to={`edit/${product.id}`}
                    className="button is-warning is-small mr-2"
                  >
                    Edit
                  </Link>
                  <button
                    onClick={() => dispatch(deleteProduct(product.id))}
                    className="button is-danger is-small"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ShowProduct;
